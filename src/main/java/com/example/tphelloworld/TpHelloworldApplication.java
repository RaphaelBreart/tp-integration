package com.example.tphelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpHelloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpHelloworldApplication.class, args);
	}

}
